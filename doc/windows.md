# Windows

This instruction is based on Microsoft Visual Studio 2022.

## Microsoft Visual Studio

1. Launch the Visual Studio Installer
2. Select `Desktop Development with C++`
3. In Installation details select `C++ Clang tools for Windows` (for `clang-format`)

## Python

1. Go to [python.org](https://www.python.org/)
2. Download the latest Windows x64 installer
3. Install and add to PATH
