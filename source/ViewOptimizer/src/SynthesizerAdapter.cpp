/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019-2025, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <TMIV/ViewOptimizer/SynthesizerAdapter.h>

#include <TMIV/Common/Factory.h>

using namespace std::string_literals;

namespace TMIV::ViewOptimizer {
namespace {
[[nodiscard]] auto log2PatchPackingBlockSize(Common::Vec2i frameSize) {
  auto ppbs = 128;

  for (uint8_t result = 7; result != 0; --result) {
    if (frameSize.x() % ppbs == 0 && frameSize.y() % ppbs == 0) {
      return result;
    }
    ppbs /= 2;
  }

  MIVBITSTREAM_ERROR("Patch packing block size has to be in 2, 4, 8 ... 128");
}

[[nodiscard]] auto asps(Common::Vec2i frameSize) {
  return MivBitstream::AtlasSequenceParameterSetRBSP{}
      .asps_frame_width(frameSize.x())
      .asps_frame_height(frameSize.y())
      .asps_log2_patch_packing_block_size(log2PatchPackingBlockSize(frameSize));
}

[[nodiscard]] auto occFrame(Common::Vec2i frameSize) {
  auto f = Common::Frame<bool>::lumaOnly(frameSize);
  f.fillOne();
  return f;
}

[[nodiscard]] auto patchParams(MivBitstream::ViewId viewId, Common::Vec2i frameSize,
                               int32_t geoBitDepth) {
  return std::vector{
      MivBitstream::PatchParams{}
          .atlasPatchProjectionId(viewId)
          .atlasPatchOrientationIndex(MivBitstream::FlexiblePatchOrientation::FPO_NULL)
          .atlasPatch2dSizeX(frameSize.x())
          .atlasPatch2dSizeY(frameSize.y())
          .atlasPatch3dRangeD(Common::maxLevel(geoBitDepth))};
}

[[nodiscard]] auto blockToPatchMap(const MivBitstream::AtlasSequenceParameterSetRBSP &asps) {
  return Common::Frame<>::lumaOnly(
      {asps.asps_frame_width() >> asps.asps_log2_patch_packing_block_size(),
       asps.asps_frame_height() >> asps.asps_log2_patch_packing_block_size()});
}

[[nodiscard]] auto adaptAltasAccessUnit(const Common::DeepFrame &frame,
                                        MivBitstream::ViewId viewId) {
  auto aau = MivBitstream::AtlasAccessUnit{};

  aau.asps = asps(frame.texture.getSize());
  aau.blockToPatchMap = blockToPatchMap(aau.asps);
  aau.texFrame = yuv444(frame.texture);
  aau.geoFrame = yuv400(frame.geometry);
  aau.occFrame = occFrame(frame.texture.getSize());
  aau.patchParamsList = patchParams(viewId, frame.texture.getSize(),
                                    Common::downCast<int32_t>(frame.geometry.getBitDepth()));

  return aau;
}

[[nodiscard]] auto adaptAccessUnit(const SourceParams &params, const Common::DeepFrameList &frame) {
  PRECONDITION(params.viewParamsList.size() == frame.size());

  auto au = MivBitstream::AccessUnit{};

  au.viewParamsList = params.viewParamsList;
  au.atlas.resize(frame.size());

  for (size_t viewIdx = 0; viewIdx < frame.size(); ++viewIdx) {
    au.atlas[viewIdx] = adaptAltasAccessUnit(frame[viewIdx], params.viewParamsList[viewIdx].viewId);
  }

  au.casps = MivBitstream::CommonAtlasSequenceParameterSetRBSP{};
  au.casps->casps_miv_extension().casme_depth_low_quality_flag(params.depthLowQualityFlag);

  return au;
}
} // namespace

SynthesizerAdapter::SynthesizerAdapter(const Common::Json &rootNode,
                                       const Common::Json &componentNode)
    : m_synthesizer{
          Common::create<Renderer::ISynthesizer>("Synthesizer"s, rootNode, componentNode)} {
  PRECONDITION(!m_synthesizer->isOptimizedForRestrictedGeometry());
}

auto SynthesizerAdapter::renderFrame(const SourceParams &params, const Common::DeepFrameList &frame,
                                     const MivBitstream::CameraConfig &cameraConfig) const
    -> Common::RendererFrame {
  return m_synthesizer->renderFrame(adaptAccessUnit(params, frame), cameraConfig);
}
} // namespace TMIV::ViewOptimizer
