/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019-2025, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <TMIV/ViewOptimizer/SynthesizeCenterView.h>

#include <TMIV/Common/Factory.h>
#include <TMIV/Common/Json.h>
#include <TMIV/Common/LoggingStrategyFmt.h>
#include <TMIV/Common/Math.h>
#include <TMIV/MivBitstream/AccessUnit.h>
#include <TMIV/MivBitstream/DepthOccupancyTransform.h>
#include <TMIV/MivBitstream/Formatters.h>
#include <TMIV/Renderer/AffineTransform.h>
#include <TMIV/Renderer/IInpainter.h>
#include <TMIV/Renderer/Projector.h>
#include <TMIV/ViewOptimizer/IViewSynthesizer.h>

#include <numeric>

using namespace std::string_literals;

namespace TMIV::ViewOptimizer {
enum CenterPoseMethod : uint8_t {
  mean,  // Average the position and orientation over all source views
  medoid // Select the pose of the most central source view
};

class SynthesizeCenterView::Impl {
private:
  std::unique_ptr<IViewSynthesizer> m_synthesizer;
  std::unique_ptr<Renderer::IInpainter> m_inpainter;
  SourceParams m_sourceParams;
  ViewOptimizerParams m_transportParams;
  int32_t m_blockSize;
  std::optional<Common::Vec2i> m_atlasFrameSize;
  std::optional<int32_t> m_maxLumaPictureSize;
  std::optional<size_t> m_centralSourceViewIdx;

public:
  Impl(const Common::Json &rootNode, const Common::Json &componentNode)
      : m_synthesizer{Common::create<IViewSynthesizer>("Synthesizer"s, rootNode, componentNode)}
      , m_inpainter{Common::create<Renderer::IInpainter>("Inpainter"s, rootNode, componentNode)}
      , m_blockSize{rootNode.require("blockSize").as<int32_t>()} {
    if (const auto &node = rootNode.optional("overrideAtlasFrameSizes")) {
      const auto &array_ = node.as<Common::Json::Array>();
      VERIFY(!array_.empty());
      m_atlasFrameSize = array_.front().asVec<int32_t, 2>();
    } else {
      m_maxLumaPictureSize = rootNode.require("maxLumaPictureSize").as<int32_t>();
      VERIFY(0 < m_maxLumaPictureSize);
    }
  }

  auto optimizeParams(const SourceParams &params) -> ViewOptimizerParams {
    m_sourceParams = params;
    std::tie(m_transportParams.viewParamsList, m_centralSourceViewIdx) = optimizeViewParamsList();
    return m_transportParams;
  }

private: // optimizeParams() implementation
  [[nodiscard]] auto optimizeViewParamsList() const
      -> std::tuple<MivBitstream::ViewParamsList, std::optional<size_t>> {
    auto vpl = MivBitstream::ViewParamsList{};

    vpl.reserve(1 + m_sourceParams.viewParamsList.size());
    vpl.push_back(optimizeCenterViewParams());

    auto centralSourceViewIdx = detectCentralSourceView(vpl.back().pose.position);

    if (centralSourceViewIdx) {
      Common::logInfo("Detected {} as the central source view.",
                      m_sourceParams.viewParamsList[*centralSourceViewIdx].name);
    } else {
      Common::logVerbose("Did not detect a central source view.");
    }

    for (size_t i = 0; i < m_sourceParams.viewParamsList.size(); ++i) {
      if (i != centralSourceViewIdx) {
        auto &vp = vpl.emplace_back(m_sourceParams.viewParamsList[i]);
        vp.isBasicView = false;
      }
    }

    vpl.constructViewIdIndex();

    return {vpl, centralSourceViewIdx};
  }

  [[nodiscard]] auto optimizeCenterViewParams() const -> MivBitstream::ViewParams {
    auto vp = MivBitstream::ViewParams{};

    const auto pose = optimizeCenterViewPose();
    const auto frustum = optimizeSphericalFrustum(pose);

    // TODO(m71483): Use perspective projection for smaller FOV

    vp.viewId = centerViewId();
    vp.pose = pose;
    vp.ci = optimizeCameraIntrinsicsERP(frustum);
    vp.dq = optimizeDepthQuantizationERP(frustum);
    vp.name = "center"s;
    vp.isBasicView = true;

    return vp;
  }

  [[nodiscard]] auto centerViewId() const -> MivBitstream::ViewId {
    for (uint16_t v = 0;; ++v) {
      const auto id = MivBitstream::ViewId{v};

      if (std::none_of(m_sourceParams.viewParamsList.cbegin(), m_sourceParams.viewParamsList.cend(),
                       [id](const auto &viewParams) { return viewParams.viewId == id; })) {
        return id;
      }
    }
  }

  [[nodiscard]] auto optimizeCenterViewPose() const -> MivBitstream::Pose {
    const auto pose = MivBitstream::Pose{
        meanOfVectors(mapRange(m_sourceParams.viewParamsList, viewPosition)),
        directAveragingOfOrientations(mapRange(m_sourceParams.viewParamsList, viewOrientation))};

    Common::logInfo("Center view pose: {}, {}", pose.position, pose.orientation);
    return pose;
  }

  [[nodiscard]] static auto viewPosition(const MivBitstream::ViewParams &vp) -> Common::Vec3f {
    return vp.pose.position;
  }

  [[nodiscard]] static auto viewOrientation(const MivBitstream::ViewParams &vp) -> Common::QuatD {
    return vp.pose.orientation;
  }

  template <typename Range, typename Func,
            typename R = decltype(std::declval<Func>()(std::declval<Range>().front()))>
  [[nodiscard]] static auto mapRange(const Range &range, Func f) -> std::vector<R> {
    auto result = std::vector<R>(range.size());
    std::transform(range.cbegin(), range.cend(), result.begin(), std::move(f));
    return result;
  }

  [[nodiscard]] static auto
  meanOfVectors(const std::vector<Common::Vec3f> &values) -> Common::Vec3f {
    return Common::Vec3f{Common::floatCast,
                         std::accumulate(values.cbegin(), values.cend(), Common::Vec3d{},
                                         [](auto a, auto b) { return a + b; }) /
                             static_cast<double>(values.size())};
  }

  struct SphericalCoordinate {
    float radius{};
    float phi_deg{};
    float theta_deg{};
  };

  struct SphericalFrustum {
    SphericalCoordinate min{};
    SphericalCoordinate max{};
    size_t count{};

    [[nodiscard]] friend auto operator+(SphericalFrustum a,
                                        SphericalFrustum b) -> SphericalFrustum {
      return {{std::min(a.min.radius, b.min.radius), std::min(a.min.phi_deg, b.min.phi_deg),
               std::min(a.min.theta_deg, b.min.theta_deg)},
              {std::max(a.max.radius, b.max.radius), std::max(a.max.phi_deg, b.max.phi_deg),
               std::max(a.max.theta_deg, b.max.theta_deg)},
              a.count + b.count};
    }

    auto operator<<(SphericalCoordinate x) -> SphericalFrustum & {
      if (count++ != 0) {
        std::tie(min.radius, max.radius) = std::minmax({min.radius, max.radius, x.radius});
        std::tie(min.phi_deg, max.phi_deg) = std::minmax({min.phi_deg, max.phi_deg, x.phi_deg});
        std::tie(min.theta_deg, max.theta_deg) =
            std::minmax({min.theta_deg, max.theta_deg, x.theta_deg});
      } else {
        min = x;
        max = x;
      }
      return *this;
    }

    explicit operator bool() const { return 0 < count; }

    [[nodiscard]] auto solidAngle() const {
      const auto result = (max.phi_deg - min.phi_deg) *
                          (Common::sinDeg(max.theta_deg) - Common::sinDeg(min.theta_deg));

      Common::logDebug(
          "SphericalFrustum::solidAngle: phi in [{}, {}] deg, theta in [{}, {}] deg, result={}",
          min.phi_deg, max.phi_deg, min.theta_deg, max.theta_deg, result);

      return result;
    }

    void log() const {
      Common::logDebug("SphericalCoordinate: [{}, {}, {}] -- [{}, {}, {}] {}", min.radius,
                       min.phi_deg, min.theta_deg, max.radius, max.phi_deg, max.theta_deg, count);
    }
  };

  [[nodiscard]] auto
  optimizeSphericalFrustum(const MivBitstream::Pose &pose) const -> SphericalFrustum {
    auto result = std::accumulate(
        std::next(m_sourceParams.viewParamsList.cbegin()), m_sourceParams.viewParamsList.cend(),
        estimateSphericalFrustum(m_sourceParams.viewParamsList.front(), pose),
        [pose](auto init, const auto &vp) { return init + estimateSphericalFrustum(vp, pose); });

    result.min.phi_deg = 10.F * std::floor(0.1F * result.min.phi_deg);
    result.max.phi_deg = 10.F * std::ceil(0.1F * result.max.phi_deg);
    result.min.theta_deg = 10.F * std::floor(0.1F * result.min.theta_deg);
    result.max.theta_deg = 10.F * std::ceil(0.1F * result.max.theta_deg);

    POSTCONDITION(-180.F <= result.min.phi_deg && result.max.phi_deg <= 180.F);
    POSTCONDITION(-90.F <= result.min.theta_deg && result.max.theta_deg <= 90.F);

    result.log();
    return result;
  }

  [[nodiscard]] static auto
  estimateSphericalFrustum(const MivBitstream::ViewParams &vp,
                           const MivBitstream::Pose &pose) -> SphericalFrustum {
    auto result = SphericalFrustum{};

    constexpr auto minNormDisp = 1E-6F; // 100 m if scene unit is mm or 100 km if scene unit is m
    constexpr auto samplingBitDepth = 3;
    constexpr auto samplingMaxLevel = Common::maxLevel(samplingBitDepth);

    const auto projector = Renderer::makeProjector(vp.ci);
    const auto projectionPlaneSize = vp.ci.projectionPlaneSizeF();
    const auto depthTransform = MivBitstream::DepthTransform{vp.dq, samplingBitDepth};
    const auto affineTransform = Renderer::AffineTransform{vp.pose, pose};

    for (uint8_t i = 0; i <= samplingMaxLevel; ++i) {
      for (uint8_t j = 0; j <= samplingMaxLevel; ++j) {
        for (uint8_t k = 0; k <= samplingMaxLevel; ++k) {
          const auto uv = Common::Vec2f{
              static_cast<float>(i) * projectionPlaneSize.x() / float{samplingMaxLevel},
              static_cast<float>(j) * projectionPlaneSize.y() / float{samplingMaxLevel}};
          const auto normDisp = std::max(minNormDisp, depthTransform.expandNormDisp(k));
          result << sphericalCoordinate(
              affineTransform(projector->unprojectVertex(uv, 1.F / normDisp)));
        }
      }
    }

    result.log();
    POSTCONDITION(result);
    return result;
  }

  [[nodiscard]] static auto sphericalCoordinate(Common::Vec3f position) -> SphericalCoordinate {
    return {norm(position), Common::atan2Deg(position.y(), position.x()),
            Common::atan2Deg(position.z(), std::hypot(position.x(), position.y()))};
  }

  [[nodiscard]] auto optimizeCameraIntrinsicsERP(const SphericalFrustum &frustum) const
      -> MivBitstream::CameraIntrinsics {
    const auto projectionPlaneSize = optimizeProjectionPlaneSizeERP(frustum);

    return MivBitstream::CameraIntrinsics{}
        .ci_projection_plane_width_minus1(projectionPlaneSize.x() - 1)
        .ci_projection_plane_height_minus1(projectionPlaneSize.y() - 1)
        .ci_cam_type(MivBitstream::CiCamType::equirectangular)
        .ci_erp_phi_min(frustum.min.phi_deg)
        .ci_erp_phi_max(frustum.max.phi_deg)
        .ci_erp_theta_min(frustum.min.theta_deg)
        .ci_erp_theta_max(frustum.max.theta_deg);
  }

  [[nodiscard]] auto
  optimizeProjectionPlaneSizeERP(const SphericalFrustum &frustum) const -> Common::Vec2i {
    const auto pixelCount = angularResolutionOfSource() * frustum.solidAngle();
    const auto aspectRatio = (frustum.max.phi_deg - frustum.min.phi_deg) /
                             (frustum.max.theta_deg - frustum.min.theta_deg);

    auto result = Common::Vec2i{alignedRound(std::sqrt(pixelCount * aspectRatio)),
                                alignedRound(std::sqrt(pixelCount / aspectRatio))};

    auto scale = std::optional<float>{};

    if (m_atlasFrameSize) {
      scale = fitProjectionPlaneToAtlasFrameSize(result, *m_atlasFrameSize);
    } else if (m_maxLumaPictureSize) {
      scale = fitProjectionPlaneSizeToMaxLumaPictureSize(result, *m_maxLumaPictureSize);
    }

    if (scale) {
      result.x() = alignedFloor(*scale * static_cast<float>(result.x()));
      result.y() = alignedFloor(*scale * static_cast<float>(result.y()));
    }

    Common::logInfo("Center view projection plane size: {}", result);

    POSTCONDITION(0 < result.x() * result.y());

    return result;
  }

  [[nodiscard]] auto angularResolutionOfSource() const -> float {
    const auto result = std::accumulate(
        m_sourceParams.viewParamsList.cbegin(), m_sourceParams.viewParamsList.cend(), 0.F,
        [](float init, const auto &vp) { return std::max(init, angularResolutionOfView(vp.ci)); });

    Common::logDebug("angularResolutionOfSource: result={}", result);

    return result;
  }

  [[nodiscard]] static auto
  fitProjectionPlaneToAtlasFrameSize(Common::Vec2i size,
                                     Common::Vec2i atlasFrameSize) -> std::optional<float> {
    if (atlasFrameSize.x() < size.x() || atlasFrameSize.y() < size.y()) {
      const auto scale =
          std::min(static_cast<float>(atlasFrameSize.x()) / static_cast<float>(size.x()),
                   static_cast<float>(atlasFrameSize.y()) / static_cast<float>(size.y()));
      Common::logWarning("The calculated center view ({} x {}) does not fit in the provided atlas "
                         "frame size ({} x {}). Scaling down by {}.",
                         size.x(), size.y(), atlasFrameSize.x(), atlasFrameSize.y(), scale);
      return scale;
    }

    return std::nullopt;
  }

  [[nodiscard]] static auto
  fitProjectionPlaneSizeToMaxLumaPictureSize(Common::Vec2i size,
                                             int32_t maxLumaPictureSize) -> std::optional<float> {
    if (maxLumaPictureSize < size.x() * size.y()) {
      const auto scale = std::sqrt(static_cast<float>(maxLumaPictureSize) /
                                   static_cast<float>(size.x() * size.y()));
      Common::logWarning("The calculated center view ({} x {}) does not fit in the provided "
                         "maximmum luma picture size ({}). Scaling down by {}.",
                         size.x(), size.y(), maxLumaPictureSize, scale);
      return scale;
    }

    return std::nullopt;
  }

  [[nodiscard]] auto alignedRound(float value) const -> int32_t {
    return Common::downCast<int32_t>(std::lround(value / static_cast<float>(m_blockSize)) *
                                     m_blockSize);
  }

  [[nodiscard]] auto alignedFloor(float value) const -> int32_t {
    return Common::downCast<int32_t>(
        std::lround(std::floor(value / static_cast<float>(m_blockSize))) * m_blockSize);
  }

  [[nodiscard]] static auto
  angularResolutionOfView(const MivBitstream::CameraIntrinsics &ci) -> float {
    const auto result = ci.projectionPlaneSizeF().x() * ci.projectionPlaneSizeF().y() /
                        sphericalFrustumOfView(ci).solidAngle();

    Common::logDebug("angularResolutionOfView: result={}", result);

    return result;
  }

  [[nodiscard]] static auto
  sphericalFrustumOfView(const MivBitstream::CameraIntrinsics &ci) -> SphericalFrustum {
    if (ci.ci_cam_type() == MivBitstream::CiCamType::equirectangular) {
      return {{0.F, ci.ci_erp_phi_min(), ci.ci_erp_theta_min()},
              {0.F, ci.ci_erp_phi_max(), ci.ci_erp_theta_max()},
              1};
    }

    if (ci.ci_cam_type() == MivBitstream::CiCamType::perspective) {
      const auto projectionPlaneSize = ci.projectionPlaneSizeF();
      const auto phi =
          Common::atanDeg(projectionPlaneSize.x() / (2.F * ci.ci_perspective_focal_hor()));
      const auto theta =
          Common::atanDeg(projectionPlaneSize.y() / (2.F * ci.ci_perspective_focal_ver()));
      return {{0.F, -phi, -theta}, {0.F, phi, theta}, 1};
    }

    UNREACHABLE;
  }

  [[nodiscard]] static auto
  optimizeDepthQuantizationERP(const SphericalFrustum &frustum) -> MivBitstream::DepthQuantization {
    PRECONDITION(0.F < frustum.min.radius);

    return MivBitstream::DepthQuantization{}
        .dq_norm_disp_low(1.F / frustum.max.radius)
        .dq_norm_disp_high(1.F / frustum.min.radius);
  }

  [[nodiscard]] auto
  detectCentralSourceView(const Common::Vec3f position) const -> std::optional<size_t> {
    const auto &vpl = m_sourceParams.viewParamsList;

    auto threshold = sourceViewsBaseline();
    auto index = std::optional<size_t>{};
    Common::logDebug("detectCentralSourceView: threshold = {}, index = nullopt", threshold);

    for (size_t i = 0; i < vpl.size(); ++i) {
      const auto distance = norm(vpl[i].pose.position - position);

      if (distance < threshold) {
        threshold = distance;
        index = i;
        Common::logDebug("detectCentralSourceView: threshold = {}, index = {}", threshold, i);
      }
    }

    return index;
  }

  [[nodiscard]] auto sourceViewsBaseline() const -> float {
    auto baseline = std::numeric_limits<float>::infinity();
    const auto &vpl = m_sourceParams.viewParamsList;

    for (size_t i = 0; i < vpl.size(); ++i) {
      for (size_t j = i + 1; j < vpl.size(); ++j) {
        baseline = std::min(baseline, norm(vpl[i].pose.position - vpl[j].pose.position));
      }
    }

    return baseline;
  }

public:
  [[nodiscard]] auto optimizeFrame(Common::DeepFrameList frame) const -> Common::DeepFrameList {
    const auto centerViewParams = centerViewCameraConfig(frame);

    auto centerFrame = m_synthesizer->renderFrame(m_sourceParams, frame, centerViewParams);

    m_inpainter->inplaceInpaint(centerFrame, centerViewParams.viewParams);

    if (m_centralSourceViewIdx) {
      frame.erase(frame.begin() + Common::downCast<ptrdiff_t>(*m_centralSourceViewIdx));
    }

    frame.insert(frame.begin(), {Common::yuv420(centerFrame.texture), centerFrame.geometry});

    return frame;
  }

private: // optimizeFrame() implementation
  [[nodiscard]] auto
  centerViewCameraConfig(const Common::DeepFrameList &frame) const -> MivBitstream::CameraConfig {
    auto cc = MivBitstream::CameraConfig{};

    cc.viewParams = m_transportParams.viewParamsList.front();

    for (const auto &view : frame) {
      cc.bitDepthTexture = std::max(cc.bitDepthTexture, view.texture.getBitDepth());
      cc.bitDepthGeometry = std::max(cc.bitDepthGeometry, view.geometry.getBitDepth());
    }

    return cc;
  }
};

SynthesizeCenterView::SynthesizeCenterView(const Common::Json &rootNode,
                                           const Common::Json &componentNode)
    : m_impl{new Impl{rootNode, componentNode}} {}

SynthesizeCenterView::~SynthesizeCenterView() = default;

auto SynthesizeCenterView::optimizeParams(const SourceParams &params) -> ViewOptimizerParams {
  return m_impl->optimizeParams(params);
}

auto SynthesizeCenterView::optimizeFrame(Common::DeepFrameList frame) const
    -> Common::DeepFrameList {
  return m_impl->optimizeFrame(std::move(frame));
}
} // namespace TMIV::ViewOptimizer
