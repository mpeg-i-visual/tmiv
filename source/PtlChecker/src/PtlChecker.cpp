/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019-2025, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

// TODO(BK): Update table numbers.

#include <TMIV/PtlChecker/PtlChecker.h>

#include <TMIV/Common/LoggingStrategyFmt.h>
#include <TMIV/MivBitstream/Formatters.h>

namespace TMIV::PtlChecker {
using CF = Common::ColorFormat;
using CG = MivBitstream::PtlProfileCodecGroupIdc;
using TS = MivBitstream::PtlProfileToolsetIdc;
using RC = MivBitstream::PtlProfileReconstructionIdc;
using LV = MivBitstream::PtlLevelIdc;
using VUT = MivBitstream::VuhUnitType;
using NUT = MivBitstream::NalUnitType;
using ATI = MivBitstream::AiAttributeTypeId;
using APM = MivBitstream::AtduPatchMode;
using Common::contains;
using Common::downCast;
using Common::logVerbose;

namespace {
void defaultLogger(const std::string &failure) {
  Common::logWarning("A profile-tier-level check has failed: {}. From this point onwards behaviour "
                     "is undefined. Do not report any subsequent errors.",
                     failure);
}
} // namespace

PtlChecker::PtlChecker() : m_logger{&defaultLogger} {}

void PtlChecker::replaceLogger(Logger value) {
  if (m_checker) {
    m_checker->replaceLogger(value);
  }

  m_logger = std::move(value);
}

void PtlChecker::checkVuh(const MivBitstream::V3cUnitHeader &vuh) {
  if (m_checker) {
    m_checker->checkVuh(vuh);
  }
}

void PtlChecker::checkNuh(const MivBitstream::NalUnitHeader &nuh) {
  if (m_checker) {
    m_checker->checkNuh(nuh);
  } else if (nuh.nal_temporal_id_plus1() != 1) {
    m_logger("nal_temporal_id_plus1 != 1");
  }
}

void PtlChecker::checkAndActivateVps(const MivBitstream::V3cParameterSet &vps) {
  m_checker = createChecker(vps);

  if (m_checker) {
    m_checker->replaceLogger(m_logger);
    m_checker->checkAndActivateVps(vps);
  }
}

void PtlChecker::checkAndActivateCasps(
    const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) {
  if (m_checker) {
    m_checker->checkAndActivateCasps(casps);
  }
}

void PtlChecker::checkAsps(MivBitstream::AtlasId atlasId,
                           const MivBitstream::AtlasSequenceParameterSetRBSP &asps) {
  if (m_checker) {
    m_checker->checkAsps(atlasId, asps);
  }
}

void PtlChecker::checkAfps(const MivBitstream::AtlasFrameParameterSetRBSP &afps) {
  if (m_checker) {
    m_checker->checkAfps(afps);
  }
}

void PtlChecker::checkAtl(const MivBitstream::NalUnitHeader &nuh,
                          const MivBitstream::AtlasTileLayerRBSP &atl) {
  if (m_checker) {
    m_checker->checkAtl(nuh, atl);
  }
}

void PtlChecker::checkCaf(const MivBitstream::NalUnitHeader &nuh,
                          const MivBitstream::CommonAtlasFrameRBSP &caf) {
  if (m_checker) {
    m_checker->checkCaf(nuh, caf);
  }
}

void PtlChecker::checkVideoFrame(MivBitstream::VuhUnitType vut,
                                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps,
                                 const Common::Frame<> &frame) {
  if (m_checker) {
    m_checker->checkVideoFrame(vut, asps, frame);
  }
}

void PtlChecker::checkV3cFrame(const MivBitstream::AccessUnit &frame) {
  if (m_checker) {
    m_checker->checkV3cFrame(frame);
  }
}

namespace {
class V3cChecker : public AbstractChecker {
protected:
  // A macro is used to capture the text of the condition. There is no reflection in C++17.
#define PTL_CHECK(document, numberedItem, condition)                                               \
  ptlCheck(condition, #condition, document, numberedItem)

  template <typename... Integer> [[nodiscard]] auto levelLimit(Integer... limit) {
    return levelLimit(std::array<std::common_type_t<Integer...>, sizeof...(Integer)>{limit...});
  }

  template <typename Integer> [[nodiscard]] auto levelLimit(const std::array<Integer, 4> &limits) {
    const auto level = ptl().ptl_level_idc();

    if (level <= LV::Level_1_5) {
      return limits[0];
    }
    if (level <= LV::Level_2_5) {
      return limits[1];
    }
    if (level <= LV::Level_3_5) {
      return limits[2];
    }
    if (level <= LV::Level_4_5) {
      return limits[3];
    }
    return std::numeric_limits<Integer>::max();
  }

  template <typename Integer> [[nodiscard]] auto levelLimit(const std::array<Integer, 8> &limits) {
    const auto level = ptl().ptl_level_idc();

    if (level <= LV::Level_1_0) {
      return limits[0];
    }
    if (level <= LV::Level_1_5) {
      return limits[1];
    }
    if (level <= LV::Level_2_0) {
      return limits[2];
    }
    if (level <= LV::Level_2_5) {
      return limits[3];
    }
    if (level <= LV::Level_3_0) {
      return limits[4];
    }
    if (level <= LV::Level_3_5) {
      return limits[5];
    }
    if (level <= LV::Level_4_0) {
      return limits[6];
    }
    if (level <= LV::Level_4_5) {
      return limits[7];
    }
    return std::numeric_limits<Integer>::max();
  }

  template <typename Integer> [[nodiscard]] auto levelLimit(const std::array<Integer, 11> &limits) {
    const auto level = ptl().ptl_level_idc();

    if (level <= LV::Level_1_0) {
      return limits[0];
    }
    if (level <= LV::Level_1_1) {
      return limits[1];
    }
    if (level <= LV::Level_2_0) {
      return limits[2];
    }
    if (level <= LV::Level_2_1) {
      return limits[3];
    }
    if (level <= LV::Level_2_2) {
      return limits[4];
    }
    if (level <= LV::Level_3_0) {
      return limits[5];
    }
    if (level <= LV::Level_3_1) {
      return limits[6];
    }
    if (level <= LV::Level_3_2) {
      return limits[7];
    }
    if (level <= LV::Level_4_0) {
      return limits[8];
    }
    if (level <= LV::Level_4_1) {
      return limits[9];
    }
    if (level <= LV::Level_4_2) {
      return limits[10];
    }
    return std::numeric_limits<Integer>::max();
  }

  [[nodiscard]] auto vps() const -> const MivBitstream::V3cParameterSet & {
    PRECONDITION(m_vps.has_value());
    return *m_vps;
  }

  [[nodiscard]] auto ptl() const -> const MivBitstream::ProfileTierLevel & {
    return vps().profile_tier_level();
  }

  [[nodiscard]] auto ptci() const -> const MivBitstream::ProfileToolsetConstraintsInformation & {
    static constexpr auto inferred = MivBitstream::ProfileToolsetConstraintsInformation{};

    return ptl().ptl_toolset_constraints_present_flag()
               ? ptl().ptl_profile_toolset_constraints_information()
               : inferred;
  }

  void ptlCheck(bool condition, const char *what, const char *document, const char *numberedItem) {
    if (!condition) {
      fail(TMIV_FMT::format("{} [{} {}]", what, document, numberedItem));
    }
  }

  template <typename Value, typename Limit>
  auto levelCheck(const char *document, const char *numberedItem, const char *identifier,
                  Value value, Limit limit) {
    using Integer = std::common_type_t<Value, Limit>;

    if (Integer{value} <= Integer{limit}) {
      logVerbose("Level check: {} <= {} = {}", value, identifier, limit);
    } else {
      fail(TMIV_FMT::format("{} <= {} = {} [{} {}]", value, identifier, limit, document,
                            numberedItem));
    }
    return value;
  }

  void fail(const std::string &failure) {
    if (m_logger) {
      m_logger(failure);
      m_logger = nullptr;
    }
  }

  static constexpr auto v3cSpec = "ISO/IEC 23090-5";
  static constexpr auto mivSpec = "ISO/IEC 23090-12";

public:
  void replaceLogger(Logger value) override { m_logger = std::move(value); }

  void checkNuh(const MivBitstream::NalUnitHeader &nuh) override {
    PTL_CHECK(mivSpec, "A.1", nuh.nal_temporal_id_plus1() == 1);
  }

  void checkAndActivateVps(const MivBitstream::V3cParameterSet &vps) override {
    m_vps = vps;

    checkVpsCommon(vps);

    for (uint8_t k = 0; k <= vps.vps_atlas_count_minus1(); ++k) {
      checkVpsAtlas(vps, vps.vps_atlas_id(k));
    }

    if (vps.vpsMivOrMiv2ExtensionPresentFlag()) {
      checkVpsMivExtension(vps.vps_miv_extension());
    }

    if (vps.vpsMiv2ExtensionPresentFlag()) {
      checkVpsMiv2Extension(vps.vps_miv_2_extension());
    }
  }

protected:
  virtual void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) {
    PTL_CHECK(v3cSpec, "Table A-1",
              contains(MivBitstream::knownCodecGroupIdcs, ptl().ptl_profile_codec_group_idc()));
    PTL_CHECK(v3cSpec, "Table A-3",
              contains(MivBitstream::knownToolsetIdcs, ptl().ptl_profile_toolset_idc()));
    PTL_CHECK(
        v3cSpec, "Table H-4",
        contains(MivBitstream::knownReconstructionIdcs, ptl().ptl_profile_reconstruction_idc()));
    PTL_CHECK(mivSpec, "A.4.3",
              ptl().ptl_level_idc() != LV::Level_8_5 || ptci().ptc_one_v3c_frame_only_flag());
    PTL_CHECK(v3cSpec, "A.6.2, Table A-5",
              contains(MivBitstream::knownLevelIdcs, ptl().ptl_level_idc()));
    PTL_CHECK(v3cSpec, "?", !ptl().ptl_tier_flag());
    PTL_CHECK(v3cSpec, "8.4.4.6",
              vps.vps_atlas_count_minus1() <= ptci().ptc_max_atlas_count_minus1());
  }

  virtual void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                             MivBitstream::AtlasId atlasId) {
    if (vps.profile_tier_level().ptl_profile_toolset_idc() !=
        MivBitstream::PtlProfileToolsetIdc::MIV_Simple_MPI) {
      levelCheck(v3cSpec, "Table A-5", "LevelMapCount", vps.vps_map_count_minus1(atlasId) + 1,
                 levelLimit(2, 2, 4, 4, 8, 8, 16, 16));
    }

    if (vps.vps_occupancy_video_present_flag(atlasId)) {
      checkOccupancyInformation(vps.occupancy_information(atlasId));
    }

    if (vps.vps_geometry_video_present_flag(atlasId)) {
      checkGeometryInformation(vps.geometry_information(atlasId));
    }

    if (vps.vps_attribute_video_present_flag(atlasId)) {
      checkAttributesInformation(vps.attribute_information(atlasId));
    }

    PTL_CHECK(v3cSpec, "8.4.4.6",
              vps.vps_map_count_minus1(atlasId) <= ptci().ptc_max_map_count_minus1());
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_multiple_map_streams_constraint_flag() ||
                  !vps.vps_multiple_map_streams_present_flag(atlasId));
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_restricted_geometry_flag() ||
                  !vps.vps_geometry_video_present_flag(atlasId));

    if (vps.vpsPackingInformationPresentFlag() && vps.vps_packed_video_present_flag(atlasId)) {
      PTL_CHECK(v3cSpec, "8.4.4.6",
                !ptci().ptc_restricted_geometry_flag() ||
                    !vps.packing_information(atlasId).pin_geometry_present_flag());
    }
  }

  virtual void checkOccupancyInformation(const MivBitstream::OccupancyInformation & /* oi */) {}

  virtual void checkGeometryInformation(const MivBitstream::GeometryInformation & /* gi */) {}

  virtual void checkAttributesInformation(const MivBitstream::AttributeInformation &ai) {
    const auto ai_attribute_count = ai.ai_attribute_count();

    levelCheck(v3cSpec, "Table A-5", "MaxNumAttributeCount", ai_attribute_count,
               levelLimit(1, 3, 4, 8, 16, 24, 32, 48));

    for (uint8_t i = 0; i < ai_attribute_count; ++i) {
      checkAttributeInformation(ai, i);
    }
  }

  virtual void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                         uint8_t attrIdx) {
    const auto ai_attribute_type_id = ai.ai_attribute_type_id(attrIdx);
    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);
    const auto ai_attribute_dimension_partitions_minus1 =
        ai.ai_attribute_dimension_partitions_minus1(attrIdx);

    PTL_CHECK(v3cSpec, "8.4.4.5", ai_attribute_type_id != ATI::ATTR_UNSPECIFIED);
    PTL_CHECK(v3cSpec, "8.4.4.5",
              ai_attribute_type_id != ATI::ATTR_NORMAL || ai_attribute_dimension_minus1 == 2);

    // TODO(23090-23#35): [CB08] Incorrect value for ptc_attribute_max_dimension_minus1
    // PTL_CHECK(v3cSpec, "8.4.4.6", ai_attribute_dimension_minus1 <=
    //           ptci().ptc_attribute_max_dimension_minus1());

    PTL_CHECK(v3cSpec, "8.4.4.6",
              ai_attribute_dimension_partitions_minus1 <=
                  ptci().ptc_attribute_max_dimension_partitions_minus1());
  }

  virtual void checkVpsMivExtension(const MivBitstream::VpsMivExtension & /* vme */) {}

  virtual void checkVpsMiv2Extension(const MivBitstream::VpsMiv2Extension & /* vme2 */) {}

public:
  void
  checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) override {
    m_casps = casps;

    if (casps.casps_miv_extension_present_flag()) {
      const auto &casme = casps.casps_miv_extension();

      if (casme.casme_vui_params_present_flag()) {
        const auto &vui = casme.vui_parameters();

        if (vui.vui_timing_info_present_flag()) {
          m_frameRate = static_cast<double>(vui.vui_time_scale()) /
                        static_cast<double>(vui.vui_num_units_in_tick());
        }
      }
    }
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) override {
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_eom_constraint_flag() || !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_plr_constraint_flag() || !asps.asps_plr_enabled_flag());
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_no_eight_orientations_constraint_flag() ||
                  !asps.asps_use_eight_orientations_flag());
    PTL_CHECK(v3cSpec, "8.4.4.6",
              !ptci().ptc_no_45degree_projection_patch_constraint_flag() ||
                  !asps.asps_extended_projection_enabled_flag());

    if (ptl().ptl_profile_toolset_idc() != MivBitstream::PtlProfileToolsetIdc::MIV_Simple_MPI) {
      levelCheck(v3cSpec, "Table A-6", "MaxAtlasSize",
                 asps.asps_frame_width() * asps.asps_frame_height(),
                 levelLimit(2'228'224, 8'912'896, 35'651'584, 134'217'728));
    }

    if (asps.asps_miv_extension_present_flag()) {
      checkAsme(atlasId, asps.asps_miv_extension());
    }
  }

protected:
  virtual void checkAsme(MivBitstream::AtlasId /* atlasId */,
                         const MivBitstream::AspsMivExtension & /* asme */) {}

public:
  void checkAfps(const MivBitstream::AtlasFrameParameterSetRBSP &afps) override {
    PTL_CHECK(mivSpec, "Table A.2", !afps.afps_lod_mode_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !afps.afps_raw_3d_offset_bit_count_explicit_mode_flag());
  }

  void checkAtl(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::AtlasTileLayerRBSP &atl) override {
    const auto &ath = atl.atlas_tile_header();

    static constexpr auto idrNuts = std::array{NUT::NAL_IDR_W_RADL, NUT::NAL_IDR_N_LP,
                                               NUT::NAL_GIDR_W_RADL, NUT::NAL_GIDR_N_LP};
    const auto idrCodedAtlas = contains(idrNuts, nuh.nal_unit_type());
    PTL_CHECK(v3cSpec, "A.6.1", !idrCodedAtlas || ath.ath_atlas_frm_order_cnt_lsb() == 0);
  }

  void checkCaf(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::CommonAtlasFrameRBSP &caf) override {
    PRECONDITION(m_casps.has_value());

    const auto irapCodedCommonAtlas = nuh.nal_unit_type() == NUT::NAL_CAF_IDR;
    PTL_CHECK(v3cSpec, "A.6.1",
              !irapCodedCommonAtlas || caf.caf_common_atlas_frm_order_cnt_lsb() == 0);

    if (caf.caf_miv_extension_present_flag()) {
      checkCame(nuh, caf.caf_miv_extension());
    }
  }

protected:
  virtual void checkCame(const MivBitstream::NalUnitHeader &nuh,
                         const MivBitstream::CafMivExtension &came) {
    if (m_casps->casps_miv_extension().casme_depth_quantization_params_present_flag()) {
      if (nuh.nal_unit_type() == NUT::NAL_CAF_IDR) {
        checkMvpl(came.miv_view_params_list());
      } else if (came.came_update_depth_quantization_flag()) {
        checkMvpudq(came.miv_view_params_update_depth_quantization());
      }
    }
  }

  virtual void checkMvpl(const MivBitstream::MivViewParamsList &mvpl) {
    for (uint16_t v = 0; v <= mvpl.mvp_num_views_minus1(); ++v) {
      checkDepthQuantization(mvpl.depth_quantization(v));
    }
  }

  virtual void checkMvpudq(const MivBitstream::MivViewParamsUpdateDepthQuantization &mvpudq) {
    for (uint16_t i = 0; i <= mvpudq.mvpudq_num_view_updates_minus1(); ++i) {
      checkDepthQuantization(mvpudq.depth_quantization(i));
    }
  }

  virtual void checkDepthQuantization(const MivBitstream::DepthQuantization &dq) {
    PTL_CHECK(mivSpec, "Table A.2", dq.dq_quantization_law() == 0);
  }

public:
  void checkVideoFrame(VUT vut, const MivBitstream::AtlasSequenceParameterSetRBSP &asps,
                       const Common::Frame<> &frame) override {
    PRECONDITION(!frame.empty());
    PRECONDITION(m_vps.has_value());

    const auto ptl_profile_codec_group_idc =
        m_vps->profile_tier_level().ptl_profile_codec_group_idc();

    if (ptl_profile_codec_group_idc == CG::MP4RA ||
        !contains(MivBitstream::knownCodecGroupIdcs, ptl_profile_codec_group_idc)) {
      return;
    }

    const auto colorFormat = frame.getColorFormat();
    PTL_CHECK(v3cSpec, "Table A-2",
              colorFormat == CF::YUV420 ||
                  (colorFormat == CF::YUV400 && (ptl_profile_codec_group_idc == CG::HEVC444 ||
                                                 ptl_profile_codec_group_idc == CG::VVC_Main10)) ||
                  (colorFormat == CF::YUV444 && vut == VUT::V3C_AVD &&
                   ptl_profile_codec_group_idc == CG::HEVC444));

    const auto bitDepth = frame.getBitDepth();
    PTL_CHECK(v3cSpec, "Table A-2",
              bitDepth == 8 || (bitDepth == 10 && (ptl_profile_codec_group_idc == CG::HEVC_Main10 ||
                                                   ptl_profile_codec_group_idc == CG::HEVC444 ||
                                                   ptl_profile_codec_group_idc == CG::VVC_Main10)));

    if (vut == VUT::V3C_OVD) {
      checkOccupancyVideoFrame(asps, frame);
    } else if (vut == VUT::V3C_GVD) {
      checkGeometryVideoFrame(asps, frame);
    } else if (vut == VUT::V3C_AVD) {
      checkAttributeVideoFrame(asps, frame);
    }
  }

protected:
  virtual void checkOccupancyVideoFrame(const MivBitstream::AtlasSequenceParameterSetRBSP &asps,
                                        const Common::Frame<> &frame) {
    int32_t occScaleX = 1;
    int32_t occScaleY = 1;

    if (m_vps->vpsMivOrMiv2ExtensionPresentFlag() &&
        m_vps->vps_miv_extension().vme_occupancy_scale_enabled_flag()) {
      const auto &asme = asps.asps_miv_extension();

      occScaleX = asme.asme_occupancy_scale_factor_x_minus1() + 1;
      occScaleY = asme.asme_occupancy_scale_factor_y_minus1() + 1;
    }

    const auto asmeOccupancyFrameWidth = asps.asps_frame_width() / occScaleX;
    const auto asmeOccupancyFrameHeight = asps.asps_frame_height() / occScaleY;

    PTL_CHECK(mivSpec, "A.4.1", frame.getWidth() == asmeOccupancyFrameWidth);
    PTL_CHECK(mivSpec, "A.4.1", frame.getHeight() == asmeOccupancyFrameHeight);
  }

  virtual void checkGeometryVideoFrame(const MivBitstream::AtlasSequenceParameterSetRBSP &asps,
                                       const Common::Frame<> &frame) {
    int32_t geoScaleX = 1;
    int32_t geoScaleY = 1;

    if (m_vps->vpsMivOrMiv2ExtensionPresentFlag() &&
        m_vps->vps_miv_extension().vme_geometry_scale_enabled_flag()) {
      const auto &asme = asps.asps_miv_extension();

      geoScaleX = asme.asme_geometry_scale_factor_x_minus1() + 1;
      geoScaleY = asme.asme_geometry_scale_factor_y_minus1() + 1;
    }

    const auto asmeGeometryFrameWidth = asps.asps_frame_width() / geoScaleX;
    const auto asmeGeometryFrameHeight = asps.asps_frame_height() / geoScaleY;

    PTL_CHECK(mivSpec, "A.4.1", frame.getWidth() == asmeGeometryFrameWidth);
    PTL_CHECK(mivSpec, "A.4.1", frame.getHeight() == asmeGeometryFrameHeight);
  }

  virtual void checkAttributeVideoFrame(const MivBitstream::AtlasSequenceParameterSetRBSP &asps,
                                        const Common::Frame<> &frame) {
    const auto aspsFrameWidth = asps.asps_frame_width();
    const auto aspsFrameHeight = asps.asps_frame_height();

    PTL_CHECK(mivSpec, "A.4.1", frame.getWidth() == aspsFrameWidth);
    PTL_CHECK(mivSpec, "A.4.1", frame.getHeight() == aspsFrameHeight);
  }

public:
  void checkV3cFrame(const MivBitstream::AccessUnit &frame) override {
    VERIFY(m_vps);
    PTL_CHECK(mivSpec, "A.4.3", !ptci().ptc_one_v3c_frame_only_flag() || !m_haveV3cFrame);
    m_haveV3cFrame = true;

    [[maybe_unused]] auto &variables = m_window.emplace_back();

    // NOTE(MPEG/PCC/Specs/23090-5#550): Annex A normatively references informative Annex B
    // NOTE(MPEG/PCC/Specs/23090-5#569): It is unclear how to aggregate over atlases

    if (m_vps->profile_tier_level().ptl_profile_toolset_idc() !=
        MivBitstream::PtlProfileToolsetIdc::MIV_Simple_MPI) {
      variables.numProjPatches = checkNumProjPatches(frame);
    }
    variables.numLumaSamples = checkNumLumaSamples(frame);

    while (m_frameRate < static_cast<double>(m_window.size())) {
      m_window.erase(m_window.begin());
    }

    // NOTE(MPEG/PCC/Specs/23090-5#570): Table A-6: Max CAB size and MaxAtlasBR limits are
    // unpractically high. Not implementing any test.

    if (m_vps->profile_tier_level().ptl_profile_toolset_idc() ==
        MivBitstream::PtlProfileToolsetIdc::MIV_Simple_MPI) {
      levelCheck(v3cSpec, "Table A-5", "MaxAggregateLumaSr",
                 maxPerSec(&V3cFrameVariables::numLumaSamples),
                 levelLimit(62'914'560, 133'693'440, 267'386'880, 534'773'760, 1'069'547'520,
                            1'069'547'520, 2'139'095'040, 4'278'190'080, 4'812'963'840,
                            8'556'380'160, 17'112'760'320));
    } else {
      levelCheck(v3cSpec, "Table A-6", "MaxProjPatchesPerSec",
                 maxPerSec(&V3cFrameVariables::numProjPatches),
                 levelLimit(65'536, 131'072, 524'288, 1'036'288, 2'097'152, 4'194'304, 8'388'608,
                            16'777'216));

      levelCheck(v3cSpec, "Table A-7", "MaxAggregateLumaSr",
                 maxPerSec(&V3cFrameVariables::numLumaSamples),
                 levelLimit(133'693'440, 267'386'880, 534'773'760, 1'069'547'520, 2'139'095'040,
                            4'278'190'080, 8'556'380'160, 17'112'760'320));
    }
  }

private:
  [[nodiscard]] auto checkNumProjPatches(const MivBitstream::AccessUnit &frame) -> int64_t {
    return std::accumulate(frame.atlas.cbegin(), frame.atlas.cend(), int64_t{},
                           [this](int64_t init, const auto &atlas) {
                             return init +
                                    levelCheck(v3cSpec, "Table A-6", "MaxNumProjPatches",
                                               downCast<int64_t>(atlas.patchParamsList.size()),
                                               levelLimit(2'048, 4'096, 16'384, 32'384, 65'536,
                                                          65'536, 262'140, 262'140));
                           });
  }

  struct V3cFrameVariables {
    int64_t numProjPatches;
    int64_t numLumaSamples;
  };

  template <typename Integer>
  [[nodiscard]] auto maxPerSec(Integer V3cFrameVariables::*field) const -> Integer {
    return std::accumulate(
        m_window.cbegin(), m_window.cend(), Integer{},
        [field](Integer init, const V3cFrameVariables &frame) { return init + frame.*field; });
  }

  [[nodiscard]] auto checkNumLumaSamples(const MivBitstream::AccessUnit &frame) -> int64_t {
    auto result = int64_t{};

    const auto count = [&result, this](const auto &frame_) {
      if (!frame_.empty()) {
        if (m_vps->profile_tier_level().ptl_profile_toolset_idc() ==
            MivBitstream::PtlProfileToolsetIdc::MIV_Simple_MPI) {
          result += levelCheck(
              v3cSpec, "Table A-5", "MaxPictureSize", frame_.getWidth() * frame_.getHeight(),
              levelLimit(2'097'152, 2'228'224, 8'912'896, 8'912'896, 8'912'896, 35'651'584,
                         35'651'584, 35'651'584, 142'606'336, 142'606'336, 142'606'336));
        } else {
          result += levelCheck(v3cSpec, "Table A-7", "MaxPictureSize",
                               frame_.getWidth() * frame_.getHeight(),
                               levelLimit(2'228'224, 8'912'896, 35'651'584, 142'606'336));
        }
      }
    };

    for (const auto &atlas : frame.atlas) {
      count(atlas.decOccFrame);
      count(atlas.decGeoFrame);

      for (const auto &decAttrFrame : atlas.decAttrFrame) {
        count(decAttrFrame);
      }

      count(atlas.decPckFrame);
    }

    return result;
  }

  Logger m_logger{&defaultLogger};
  std::optional<MivBitstream::V3cParameterSet> m_vps;
  std::optional<MivBitstream::CommonAtlasSequenceParameterSetRBSP> m_casps;
  bool m_haveV3cFrame{};
  double m_frameRate{30.};
  std::vector<V3cFrameVariables> m_window;
};

class VpccBasicChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(
        v3cSpec, "Table H-3",
        contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_OVD, VUT::V3C_GVD, VUT::V3C_AVD},
                 vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(v3cSpec, "Table H-3", !vps.vpsMivOrMiv2ExtensionPresentFlag());
    PTL_CHECK(v3cSpec, "Table H-3", !vps.vpsPackingInformationPresentFlag());
    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_atlas_count_minus1() == 0);
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_map_count_minus1(atlasId) <= 1);
    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_occupancy_video_present_flag(atlasId));
    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_geometry_video_present_flag(atlasId));
  }

  void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                 uint8_t attrIdx) final {
    V3cChecker::checkAttributeInformation(ai, attrIdx);

    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);

    PTL_CHECK(v3cSpec, "Table H-3", ai_attribute_dimension_minus1 == 2);
  }

  void checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) final {
    V3cChecker::checkAndActivateCasps(casps);

    PTL_CHECK(v3cSpec, "Table H-3", !casps.casps_extension_present_flag());
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_plr_enabled_flag());
    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_use_eight_orientations_flag());
    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_extended_projection_enabled_flag());
    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_miv_extension_present_flag());
  }

  void checkCaf(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::CommonAtlasFrameRBSP &caf) final {
    V3cChecker::checkCaf(nuh, caf);

    PTL_CHECK(v3cSpec, "Table H-3", caf.caf_extension_present_flag() == 0);
    PTL_CHECK(v3cSpec, "Table H-3", caf.caf_miv_extension_present_flag() == 0);
  }
};

struct VpccExtendedChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(
        v3cSpec, "Table H-3",
        contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_OVD, VUT::V3C_GVD, VUT::V3C_AVD},
                 vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(v3cSpec, "Table H-3", !vps.vpsMivOrMiv2ExtensionPresentFlag());
    PTL_CHECK(v3cSpec, "Table H-3", !vps.vpsPackingInformationPresentFlag());
    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_atlas_count_minus1() == 0);
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_occupancy_video_present_flag(atlasId));
    PTL_CHECK(v3cSpec, "Table H-3", vps.vps_geometry_video_present_flag(atlasId));
  }

  void checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) final {
    V3cChecker::checkAndActivateCasps(casps);

    PTL_CHECK(v3cSpec, "Table H-3", !casps.casps_extension_present_flag());
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(v3cSpec, "Table H-3", !asps.asps_miv_extension_present_flag());
  }

  void checkCaf(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::CommonAtlasFrameRBSP &caf) final {
    V3cChecker::checkCaf(nuh, caf);

    PTL_CHECK(v3cSpec, "Table H-3", caf.caf_extension_present_flag() == 0);
    PTL_CHECK(v3cSpec, "Table H-3", caf.caf_miv_extension_present_flag() == 0);
  }
};

struct MivMainChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(
        v3cSpec, "Table A.2",
        contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_GVD, VUT::V3C_AVD, VUT::V3C_CAD},
                 vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(mivSpec, "Table A.2",
              ptl().ptl_profile_reconstruction_idc() == RC::Rec_Unconstrained);
    PTL_CHECK(mivSpec, "Table A.2", vps.vpsMivExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.2", !vps.vpsMiv2ExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.2", !vps.vpsPackingInformationPresentFlag());
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(mivSpec, "Table A.2", vps.vps_map_count_minus1(atlasId) == 0);
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_auxiliary_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_occupancy_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.2", vps.vps_geometry_video_present_flag(atlasId));
  }

  void checkGeometryInformation(const MivBitstream::GeometryInformation &gi) final {
    const auto gi_geometry_msb_align_flag = gi.gi_geometry_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.2", !gi_geometry_msb_align_flag);
  }

  void checkAttributesInformation(const MivBitstream::AttributeInformation &ai) final {
    V3cChecker::checkAttributesInformation(ai);

    const auto ai_attribute_count = ai.ai_attribute_count();

    PTL_CHECK(mivSpec, "Table A.2", ai_attribute_count <= 1);
  }

  void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                 uint8_t attrIdx) final {
    V3cChecker::checkAttributeInformation(ai, attrIdx);

    const auto ai_attribute_type_id = ai.ai_attribute_type_id(attrIdx);
    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);
    const auto ai_attribute_msb_align_flag = ai.ai_attribute_msb_align_flag(attrIdx);

    PTL_CHECK(mivSpec, "Table A.2", ai_attribute_type_id == ATI::ATTR_TEXTURE);
    PTL_CHECK(mivSpec, "Table A.2", !ai_attribute_msb_align_flag);

    if (ai_attribute_type_id == ATI::ATTR_TEXTURE) {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_dimension_minus1 == 2);
    }
  }

  void checkVpsMivExtension(const MivBitstream::VpsMivExtension &vme) final {
    const auto vme_embedded_occupancy_enabled_flag = vme.vme_embedded_occupancy_enabled_flag();

    PTL_CHECK(mivSpec, "Table A.2", vme_embedded_occupancy_enabled_flag);
  }

  void checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) final {
    V3cChecker::checkAndActivateCasps(casps);

    PTL_CHECK(mivSpec, "Table A.2", !casps.casps_miv_2_extension_present_flag());
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_max_dec_atlas_frame_buffering_minus1());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_long_term_ref_atlas_frames_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_pixel_deinterleaving_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_patch_precedence_order_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_raw_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_plr_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_vpcc_extension_present_flag());
  }

  void checkAsme(MivBitstream::AtlasId /* atlasId */,
                 const MivBitstream::AspsMivExtension &asme) final {
    const auto asme_patch_constant_depth_flag = asme.asme_patch_constant_depth_flag();

    PTL_CHECK(mivSpec, "Table A.2", !asme_patch_constant_depth_flag);
  }

  void checkAtl(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::AtlasTileLayerRBSP &atl) final {
    V3cChecker::checkAtl(nuh, atl);

    const auto &ath = atl.atlas_tile_header();
    const auto ath_type = ath.ath_type();
    const auto &atdu = atl.atlas_tile_data_unit();

    PTL_CHECK(mivSpec, "Table A.2", ath_type == MivBitstream::AthType::I_TILE);

    for (size_t p = 0; p < atdu.atduTotalNumberOfPatches(); ++p) {
      PTL_CHECK(mivSpec, "Table A.2", atdu.atdu_patch_mode(p) == APM::I_INTRA);
    }
  }
};

struct MivExtendedChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    if (ptci().ptc_restricted_geometry_flag()) {
      PTL_CHECK(
          v3cSpec, "Table H-3",
          contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_AVD, VUT::V3C_PVD, VUT::V3C_CAD},
                   vuh.vuh_unit_type()));
    } else {
      PTL_CHECK(v3cSpec, "Table A.2",
                contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_OVD, VUT::V3C_GVD,
                                    VUT::V3C_AVD, VUT::V3C_PVD, VUT::V3C_CAD},
                         vuh.vuh_unit_type()));
    }
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(mivSpec, "Table A.2",
              ptl().ptl_profile_reconstruction_idc() == RC::Rec_Unconstrained);
    PTL_CHECK(mivSpec, "Table A.2", vps.vpsMivExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.2", !vps.vpsMiv2ExtensionPresentFlag());
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(mivSpec, "Table A.2", vps.vps_map_count_minus1(atlasId) == 0);
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_auxiliary_video_present_flag(atlasId));

    if (ptci().ptc_restricted_geometry_flag()) {
      PTL_CHECK(mivSpec, "Table A.2", !vps.vps_occupancy_video_present_flag(atlasId));
      PTL_CHECK(mivSpec, "Table A.2", !vps.vps_geometry_video_present_flag(atlasId));
    }
  }

  void checkOccupancyInformation(const MivBitstream::OccupancyInformation &oi) final {
    const auto oi_occupancy_msb_align_flag = oi.oi_occupancy_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.2", !oi_occupancy_msb_align_flag);
  }

  void checkGeometryInformation(const MivBitstream::GeometryInformation &gi) final {
    const auto gi_geometry_msb_align_flag = gi.gi_geometry_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.2", !gi_geometry_msb_align_flag);
  }

  void checkAttributesInformation(const MivBitstream::AttributeInformation &ai) final {
    V3cChecker::checkAttributesInformation(ai);

    const auto ai_attribute_count = ai.ai_attribute_count();

    if (ptci().ptc_restricted_geometry_flag()) {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_count == 2);

      if (ai_attribute_count == 2) {
        PTL_CHECK(mivSpec, "Table A.2", ai.ai_attribute_type_id(0) != ai.ai_attribute_type_id(1));
      }
    } else {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_count <= 2);
    }
  }

  void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                 uint8_t attrIdx) final {
    V3cChecker::checkAttributeInformation(ai, attrIdx);

    const auto ai_attribute_type_id = ai.ai_attribute_type_id(attrIdx);
    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);
    const auto ai_attribute_msb_align_flag = ai.ai_attribute_msb_align_flag(attrIdx);

    PTL_CHECK(mivSpec, "Table A.2",
              ai_attribute_type_id == ATI::ATTR_TEXTURE ||
                  ai_attribute_type_id == ATI::ATTR_TRANSPARENCY);
    PTL_CHECK(mivSpec, "Table A.2", !ai_attribute_msb_align_flag);

    if (ai_attribute_type_id == ATI::ATTR_TEXTURE) {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_dimension_minus1 == 2);
    } else if (ai_attribute_type_id == ATI::ATTR_TRANSPARENCY) {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_dimension_minus1 == 0);
    }
  }

  void checkVpsMivExtension(const MivBitstream::VpsMivExtension &vme) final {
    const auto vme_embedded_occupancy_enabled_flag = vme.vme_embedded_occupancy_enabled_flag();

    if (ptci().ptc_restricted_geometry_flag()) {
      PTL_CHECK(mivSpec, "Table A.2", !vme_embedded_occupancy_enabled_flag);
    }
  }

  void checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) final {
    V3cChecker::checkAndActivateCasps(casps);

    PTL_CHECK(mivSpec, "Table A.2", !casps.casps_miv_2_extension_present_flag());
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_max_dec_atlas_frame_buffering_minus1());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_long_term_ref_atlas_frames_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_pixel_deinterleaving_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_patch_precedence_order_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_raw_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_plr_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_vpcc_extension_present_flag());
  }

  void checkAsme(MivBitstream::AtlasId atlasId, const MivBitstream::AspsMivExtension &asme) final {
    const auto vps_geometry_video_present_flag = vps().vps_geometry_video_present_flag(atlasId);
    const auto asme_patch_constant_depth_flag = asme.asme_patch_constant_depth_flag();
    const auto pin_geometry_present_flag =
        vps().vps_packed_video_present_flag(atlasId) &&
        vps().packing_information(atlasId).pin_geometry_present_flag();

    PTL_CHECK(mivSpec, "Table A.2",
              vps_geometry_video_present_flag || pin_geometry_present_flag ||
                  asme_patch_constant_depth_flag);
  }

  void checkAtl(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::AtlasTileLayerRBSP &atl) final {
    V3cChecker::checkAtl(nuh, atl);

    const auto &ath = atl.atlas_tile_header();
    const auto ath_type = ath.ath_type();
    const auto &atdu = atl.atlas_tile_data_unit();

    PTL_CHECK(mivSpec, "Table A.2", ath_type == MivBitstream::AthType::I_TILE);

    for (size_t p = 0; p < atdu.atduTotalNumberOfPatches(); ++p) {
      PTL_CHECK(mivSpec, "Table A.2", atdu.atdu_patch_mode(p) == APM::I_INTRA);
    }
  }
};

struct MivGeometryAbsentChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(
        v3cSpec, "Table A.2",
        contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_AVD, VUT::V3C_PVD, VUT::V3C_CAD},
                 vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(mivSpec, "Table A.2",
              ptl().ptl_profile_reconstruction_idc() == RC::Rec_Unconstrained);
    PTL_CHECK(mivSpec, "Table A.2", vps.vpsMivExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.2", !vps.vpsMiv2ExtensionPresentFlag());
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(mivSpec, "Table A.2", vps.vps_map_count_minus1(atlasId) == 0);
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_auxiliary_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_occupancy_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.2", !vps.vps_geometry_video_present_flag(atlasId));
  }

  void checkGeometryInformation(const MivBitstream::GeometryInformation &gi) final {
    const auto gi_geometry_msb_align_flag = gi.gi_geometry_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.2", !gi_geometry_msb_align_flag);
  }

  void checkAttributesInformation(const MivBitstream::AttributeInformation &ai) final {
    V3cChecker::checkAttributesInformation(ai);

    const auto ai_attribute_count = ai.ai_attribute_count();

    PTL_CHECK(mivSpec, "Table A.2", ai_attribute_count <= 1);
  }

  void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                 uint8_t attrIdx) final {
    V3cChecker::checkAttributeInformation(ai, attrIdx);

    const auto ai_attribute_type_id = ai.ai_attribute_type_id(attrIdx);
    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);
    const auto ai_attribute_msb_align_flag = ai.ai_attribute_msb_align_flag(attrIdx);

    PTL_CHECK(mivSpec, "Table A.2", ai_attribute_type_id == ATI::ATTR_TEXTURE);
    PTL_CHECK(mivSpec, "Table A.2", !ai_attribute_msb_align_flag);

    if (ai_attribute_type_id == ATI::ATTR_TEXTURE) {
      PTL_CHECK(mivSpec, "Table A.2", ai_attribute_dimension_minus1 == 2);
    }
  }

  void checkVpsMivExtension(const MivBitstream::VpsMivExtension &vme) final {
    const auto vme_embedded_occupancy_enabled_flag = vme.vme_embedded_occupancy_enabled_flag();

    PTL_CHECK(mivSpec, "Table A.2", !vme_embedded_occupancy_enabled_flag);
  }

  void checkAndActivateCasps(const MivBitstream::CommonAtlasSequenceParameterSetRBSP &casps) final {
    V3cChecker::checkAndActivateCasps(casps);

    PTL_CHECK(mivSpec, "Table A.2", !casps.casps_miv_2_extension_present_flag());
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_max_dec_atlas_frame_buffering_minus1());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_long_term_ref_atlas_frames_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_pixel_deinterleaving_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_patch_precedence_order_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_raw_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_plr_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.2", !asps.asps_vpcc_extension_present_flag());
  }

  void checkAtl(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::AtlasTileLayerRBSP &atl) final {
    V3cChecker::checkAtl(nuh, atl);

    const auto &ath = atl.atlas_tile_header();
    const auto ath_type = ath.ath_type();
    const auto &atdu = atl.atlas_tile_data_unit();

    PTL_CHECK(mivSpec, "Table A.2", ath_type == MivBitstream::AthType::I_TILE);

    for (size_t p = 0; p < atdu.atduTotalNumberOfPatches(); ++p) {
      PTL_CHECK(mivSpec, "Table A.2", atdu.atdu_patch_mode(p) == APM::I_INTRA);
    }
  }
};

struct Miv2Checker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(v3cSpec, "Table A.3",
              contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_OVD, VUT::V3C_GVD,
                                  VUT::V3C_AVD, VUT::V3C_PVD, VUT::V3C_CAD},
                       vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(mivSpec, "Table A.3",
              ptl().ptl_profile_reconstruction_idc() == RC::Rec_Unconstrained);
    PTL_CHECK(mivSpec, "Table A.3", !vps.vpsMivExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.3", vps.vpsMiv2ExtensionPresentFlag());
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(mivSpec, "Table A.3", vps.vps_map_count_minus1(atlasId) == 0);
    PTL_CHECK(mivSpec, "Table A.3", !vps.vps_auxiliary_video_present_flag(atlasId));
  }

  void checkOccupancyInformation(const MivBitstream::OccupancyInformation &oi) final {
    const auto oi_occupancy_msb_align_flag = oi.oi_occupancy_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.3", !oi_occupancy_msb_align_flag);
  }

  void checkGeometryInformation(const MivBitstream::GeometryInformation &gi) final {
    const auto gi_geometry_msb_align_flag = gi.gi_geometry_msb_align_flag();

    PTL_CHECK(mivSpec, "Table A.3", !gi_geometry_msb_align_flag);
  }

  void checkAttributesInformation(const MivBitstream::AttributeInformation &ai) final {
    V3cChecker::checkAttributesInformation(ai);

    const auto ai_attribute_count = ai.ai_attribute_count();

    PTL_CHECK(mivSpec, "Table A.3", ai_attribute_count <= 2);
  }

  void checkAttributeInformation(const MivBitstream::AttributeInformation &ai,
                                 uint8_t attrIdx) final {
    V3cChecker::checkAttributeInformation(ai, attrIdx);

    const auto ai_attribute_type_id = ai.ai_attribute_type_id(attrIdx);
    const auto ai_attribute_dimension_minus1 = ai.ai_attribute_dimension_minus1(attrIdx);
    const auto ai_attribute_msb_align_flag = ai.ai_attribute_msb_align_flag(attrIdx);

    PTL_CHECK(mivSpec, "Table A.3",
              ai_attribute_type_id == ATI::ATTR_TEXTURE ||
                  ai_attribute_type_id == ATI::ATTR_TRANSPARENCY);
    PTL_CHECK(mivSpec, "Table A.3", !ai_attribute_msb_align_flag);

    if (ai_attribute_type_id == ATI::ATTR_TEXTURE) {
      PTL_CHECK(mivSpec, "Table A.3", ai_attribute_dimension_minus1 == 2);
    } else if (ai_attribute_type_id == ATI::ATTR_TRANSPARENCY) {
      PTL_CHECK(mivSpec, "Table A.3", ai_attribute_dimension_minus1 == 0);
    }
  }

  void checkAsps(MivBitstream::AtlasId atlasId,
                 const MivBitstream::AtlasSequenceParameterSetRBSP &asps) final {
    V3cChecker::checkAsps(atlasId, asps);

    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_max_dec_atlas_frame_buffering_minus1());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_long_term_ref_atlas_frames_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_pixel_deinterleaving_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_patch_precedence_order_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_raw_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_eom_patch_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_plr_enabled_flag());
    PTL_CHECK(mivSpec, "Table A.3", !asps.asps_vpcc_extension_present_flag());
  }

  void checkAtl(const MivBitstream::NalUnitHeader &nuh,
                const MivBitstream::AtlasTileLayerRBSP &atl) final {
    V3cChecker::checkAtl(nuh, atl);

    const auto &ath = atl.atlas_tile_header();
    const auto ath_type = ath.ath_type();
    const auto &atdu = atl.atlas_tile_data_unit();

    PTL_CHECK(mivSpec, "Table A.3", ath_type == MivBitstream::AthType::I_TILE);

    for (size_t p = 0; p < atdu.atduTotalNumberOfPatches(); ++p) {
      PTL_CHECK(mivSpec, "Table A.3", atdu.atdu_patch_mode(p) == APM::I_INTRA);
    }
  }

  void checkDepthQuantization(const MivBitstream::DepthQuantization &dq) override {
    PTL_CHECK(mivSpec, "Table A.2",
              dq.dq_quantization_law() == 0 || dq.dq_quantization_law() == 2 ||
                  dq.dq_quantization_law() == 4);
  }
};

struct MivSimpleMpiChecker final : public V3cChecker {
  void checkVuh(const MivBitstream::V3cUnitHeader &vuh) final {
    PTL_CHECK(v3cSpec, "Table A.4",
              contains(std::array{VUT::V3C_VPS, VUT::V3C_AD, VUT::V3C_PVD, VUT::V3C_CAD},
                       vuh.vuh_unit_type()));
  }

  void checkVpsCommon(const MivBitstream::V3cParameterSet &vps) final {
    V3cChecker::checkVpsCommon(vps);

    PTL_CHECK(mivSpec, "Table A.4", ptl().ptl_profile_toolset_idc() == TS::MIV_Simple_MPI);
    PTL_CHECK(mivSpec, "Table A.4",
              ptl().ptl_profile_reconstruction_idc() == RC::Rec_Unconstrained);
    PTL_CHECK(mivSpec, "Table A.4", ptci().ptc_restricted_geometry_flag());
    PTL_CHECK(mivSpec, "Table A.4", vps.vpsMivExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.4", !vps.vpsMiv2ExtensionPresentFlag());
    PTL_CHECK(mivSpec, "Table A.4", vps.vpsPackingInformationPresentFlag());
    PTL_CHECK(mivSpec, "Table A.4", vps.vps_atlas_count_minus1() == 0);
  }

  void checkVpsAtlas(const MivBitstream::V3cParameterSet &vps,
                     MivBitstream::AtlasId atlasId) final {
    V3cChecker::checkVpsAtlas(vps, atlasId);

    PTL_CHECK(mivSpec, "Table A.4", vps.vps_map_count_minus1(atlasId) == 0);
    PTL_CHECK(mivSpec, "Table A.4", !vps.vps_auxiliary_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.4", !vps.vps_occupancy_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.4", !vps.vps_geometry_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.4", !vps.vps_attribute_video_present_flag(atlasId));
    PTL_CHECK(mivSpec, "Table A.4", vps.vps_packed_video_present_flag(atlasId));
  }
};

} // namespace

[[nodiscard]] auto PtlChecker::createChecker(const MivBitstream::V3cParameterSet &vps) const
    -> std::unique_ptr<AbstractChecker> {
  const auto ptl_profile_toolset_idc = vps.profile_tier_level().ptl_profile_toolset_idc();

  switch (ptl_profile_toolset_idc) {
  case TS::VPCC_Basic:
    return std::make_unique<VpccBasicChecker>();
  case TS::VPCC_Extended:
    return std::make_unique<VpccExtendedChecker>();
  case TS::MIV_Main:
    return std::make_unique<MivMainChecker>();
  case TS::MIV_Extended:
    return std::make_unique<MivExtendedChecker>();
  case TS::MIV_Geometry_Absent:
    return std::make_unique<MivGeometryAbsentChecker>();
  case TS::MIV_2:
    return std::make_unique<Miv2Checker>();
  case TS::MIV_Simple_MPI:
    return std::make_unique<MivSimpleMpiChecker>();
  default:
    m_logger(TMIV_FMT::format("Unknown toolset IDC {}", ptl_profile_toolset_idc));
    return {};
  }
}
} // namespace TMIV::PtlChecker
