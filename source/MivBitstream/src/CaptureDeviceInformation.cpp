/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019-2025, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <TMIV/MivBitstream/CaptureDeviceInformation.h>

#include <TMIV/Common/format.h>
#include <TMIV/Common/verify.h>
#include <TMIV/MivBitstream/Formatters.h>

#include <set>

namespace TMIV::MivBitstream {
auto operator<<(std::ostream &stream, DeviceClassId x) -> std::ostream & {
  switch (x) {
  case DeviceClassId::unknown_or_processed:
    return stream << "unknown_or_processed";
  case DeviceClassId::color_camera:
    return stream << "color_camera";
  case DeviceClassId::passive_stereo:
    return stream << "passive_stereo";
  case DeviceClassId::active_stereo:
    return stream << "active_stereo";
  case DeviceClassId::structured_light:
    return stream << "structured_light";
  case DeviceClassId::time_of_flight:
    return stream << "time_of_flight";
  case DeviceClassId::lidar:
    return stream << "lidar";
  default:
    return stream << "[unknown:" << int32_t{static_cast<uint8_t>(x)} << "]";
  }
}

auto CaptureDeviceInformation::cdi_device_model_count_minus1() const noexcept -> uint16_t {
  return m_cdi_device_model_count_minus1;
}

auto CaptureDeviceInformation::cdi_device_model_count_minus1(uint16_t value)
    -> CaptureDeviceInformation & {
  m_cdi_device_model_count_minus1 = value;

  auto sz = m_cdi_device_model_id.size();
  m_cdi_device_model_id.resize(value + 1);
  for (auto m = sz; m < m_cdi_device_model_id.size(); m++) {
    m_cdi_device_model_id[m] = static_cast<uint8_t>(m);
  }
  m_cdi_device_class_id.resize(value + 1);
  m_cdi_sensor_count_minus1.resize(value + 1);
  m_cdi_sensor_component_id.resize(value + 1);
  m_cdi_intra_sensor_parallax_flag.resize(value + 1);
  m_cdi_light_source_count.resize(value + 1);
  m_cdi_infrared_image_present_flag.resize(value + 1);
  m_cdi_depth_confidence_present_flag.resize(value + 1);
  m_cdi_depth_confidence_flag.resize(value + 1);
  return *this;
}

auto CaptureDeviceInformation::cdi_device_model_id(uint16_t m) const -> uint8_t {
  VERIFY(m < m_cdi_device_model_id.size());
  return m_cdi_device_model_id[m];
}

auto CaptureDeviceInformation::cdi_device_class_id(uint16_t m) const -> DeviceClassId {
  VERIFY(m < m_cdi_device_class_id.size());
  return m_cdi_device_class_id[m];
}

auto CaptureDeviceInformation::cdi_sensor_count_minus1(uint16_t m) const -> uint16_t {
  VERIFY(m < m_cdi_sensor_count_minus1.size());
  return m_cdi_sensor_count_minus1[m];
}

auto CaptureDeviceInformation::cdi_sensor_component_id(uint16_t m, uint16_t s) const -> uint8_t {
  VERIFY(m < m_cdi_sensor_component_id.size());
  VERIFY(s < m_cdi_sensor_component_id[m].size());
  return m_cdi_sensor_component_id[m][s];
}

auto CaptureDeviceInformation::cdi_intra_sensor_parallax_flag(uint16_t m) const -> bool {
  VERIFY(m < m_cdi_intra_sensor_parallax_flag.size());
  return m_cdi_intra_sensor_parallax_flag[m];
}

auto CaptureDeviceInformation::cdi_light_source_count(uint16_t m) const -> uint16_t {
  VERIFY(m < m_cdi_light_source_count.size());
  return m_cdi_light_source_count[m];
}

auto CaptureDeviceInformation::cdi_infrared_image_present_flag(uint16_t m) const -> bool {
  VERIFY(m < m_cdi_infrared_image_present_flag.size());
  return m_cdi_infrared_image_present_flag[m];
}

auto CaptureDeviceInformation::cdi_depth_confidence_present_flag(uint16_t m) const -> bool {
  VERIFY(m < m_cdi_depth_confidence_present_flag.size());
  return m_cdi_depth_confidence_present_flag[m];
}

auto CaptureDeviceInformation::cdi_depth_confidence_flag(uint16_t m, uint16_t s) const -> bool {
  VERIFY(m < m_cdi_depth_confidence_flag.size());
  VERIFY(s < m_cdi_depth_confidence_flag[m].size());
  return m_cdi_depth_confidence_flag[m][s];
}

auto CaptureDeviceInformation::cdi_device_model_id(uint16_t m,
                                                   uint8_t value) -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_device_model_id.size());
  m_cdi_device_model_id[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_device_class_id(uint16_t m, DeviceClassId value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_device_class_id.size());
  m_cdi_device_class_id[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_sensor_count_minus1(uint16_t m, uint16_t value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_sensor_count_minus1.size());
  m_cdi_sensor_count_minus1[m] = value;

  m_cdi_sensor_component_id[m].resize(value + 1);
  m_cdi_depth_confidence_flag[m].resize(value + 1);
  return *this;
}

auto CaptureDeviceInformation::cdi_sensor_component_id(uint16_t m, uint16_t s, uint8_t value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_sensor_component_id.size());
  VERIFY(s < m_cdi_sensor_component_id[m].size());
  m_cdi_sensor_component_id[m][s] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_intra_sensor_parallax_flag(uint16_t m, bool value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_intra_sensor_parallax_flag.size());
  m_cdi_intra_sensor_parallax_flag[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_light_source_count(uint16_t m, uint16_t value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_light_source_count.size());
  m_cdi_light_source_count[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_infrared_image_present_flag(uint16_t m, bool value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_infrared_image_present_flag.size());
  m_cdi_infrared_image_present_flag[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_depth_confidence_present_flag(uint16_t m, bool value)
    -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_depth_confidence_present_flag.size());
  m_cdi_depth_confidence_present_flag[m] = value;
  return *this;
}

auto CaptureDeviceInformation::cdi_depth_confidence_flag(uint16_t m, uint16_t s,
                                                         bool value) -> CaptureDeviceInformation & {
  VERIFY(m < m_cdi_depth_confidence_flag.size());
  VERIFY(s < m_cdi_depth_confidence_flag[m].size());
  m_cdi_depth_confidence_flag[m][s] = value;
  return *this;
}

auto CaptureDeviceInformation::deviceModelIdx(uint8_t i) const -> uint16_t {
  for (uint16_t m = 0; m <= cdi_device_model_count_minus1(); ++m) {
    if (cdi_device_model_id(m) == i) {
      return m;
    }
  }

  RUNTIME_ERROR("Unknown device model ID");
}

auto CaptureDeviceInformation::deviceClassId(uint8_t i) const -> DeviceClassId {
  return cdi_device_class_id(deviceModelIdx(i));
}

auto CaptureDeviceInformation::sensorCount(uint8_t i) const -> uint16_t {
  if (deviceClassId(i) == DeviceClassId::unknown_or_processed) {
    return 0;
  }

  return Common::downCast<uint16_t>(cdi_sensor_count_minus1(deviceModelIdx(i)) + 1);
}

auto CaptureDeviceInformation::sensorComponentId(uint8_t i, uint16_t s) const -> uint8_t {
  return cdi_sensor_component_id(deviceModelIdx(i), s);
}

auto CaptureDeviceInformation::intraSensorParallaxFlag(uint8_t i) const -> bool {
  return cdi_intra_sensor_parallax_flag(deviceModelIdx(i));
}

auto CaptureDeviceInformation::lightSourceCount(uint8_t i) const -> uint16_t {
  return cdi_light_source_count(deviceModelIdx(i));
}

auto CaptureDeviceInformation::irImagePresentFlag(uint8_t i) const -> bool {
  return cdi_infrared_image_present_flag(deviceModelIdx(i));
}

auto CaptureDeviceInformation::depthConfidencePresenceFlag(uint8_t i) const -> bool {
  return cdi_depth_confidence_present_flag(deviceModelIdx(i));
}

auto CaptureDeviceInformation::depthConfidenceFlag(uint8_t i, uint16_t s) const -> bool {
  return cdi_depth_confidence_flag(deviceModelIdx(i), s);
}

auto operator<<(std::ostream &stream, const CaptureDeviceInformation &x) -> std::ostream & {
  TMIV_FMT::print(stream, "cdi_device_model_count_minus1={}\n", x.cdi_device_model_count_minus1());
  for (uint16_t m = 0; m <= x.cdi_device_model_count_minus1(); m++) {
    TMIV_FMT::print(stream, "cdi_device_model_id[{}]={}\n", m, x.cdi_device_model_id(m));
    TMIV_FMT::print(stream, "cdi_device_class_id[{}]={}\n", m, x.cdi_device_class_id(m));

    if (x.cdi_device_class_id(m) != DeviceClassId::unknown_or_processed) {
      TMIV_FMT::print(stream, "cdi_sensor_count_minus1[{}]={}\n", m, x.cdi_sensor_count_minus1(m));

      for (uint16_t s = 0; s <= x.cdi_sensor_count_minus1(m); s++) {
        TMIV_FMT::print(stream, "cdi_sensor_component_id[{}][{}]={}\n", m, s,
                        x.cdi_sensor_component_id(m, s));
      }

      TMIV_FMT::print(stream, "cdi_intra_sensor_parallax_flag[{}]={}\n", m,
                      x.cdi_intra_sensor_parallax_flag(m));
      TMIV_FMT::print(stream, "cdi_light_source_count[{}]={}\n", m, x.cdi_light_source_count(m));
      TMIV_FMT::print(stream, "cdi_infrared_image_present_flag[{}]={}\n", m,
                      x.cdi_infrared_image_present_flag(m));
      TMIV_FMT::print(stream, "cdi_depth_confidence_present_flag[{}]={}\n", m,
                      x.cdi_depth_confidence_present_flag(m));

      if (x.cdi_depth_confidence_present_flag(m)) {
        for (uint16_t s = 0; s <= x.cdi_sensor_count_minus1(m); s++) {
          if (x.cdi_sensor_component_id(m, s) == 0) {
            TMIV_FMT::print(stream, "cdi_depth_confidence_flag[{}][{}]={}\n", m, s,
                            x.cdi_depth_confidence_flag(m, s));
          }
        }
      }
    }
  }

  return stream;
}

auto CaptureDeviceInformation::operator==(const CaptureDeviceInformation &other) const -> bool {
  if (cdi_device_model_count_minus1() != other.cdi_device_model_count_minus1()) {
    return false;
  }

  for (uint16_t m = 0; m <= cdi_device_model_count_minus1(); m++) {
    if ((cdi_device_model_id(m) != other.cdi_device_model_id(m)) ||
        (cdi_device_class_id(m) != other.cdi_device_class_id(m))) {
      return false;
    }

    if (cdi_device_class_id(m) != DeviceClassId::unknown_or_processed) {
      if (cdi_sensor_count_minus1(m) != other.cdi_sensor_count_minus1(m) ||
          m_cdi_sensor_component_id[m] != other.m_cdi_sensor_component_id[m] ||
          cdi_intra_sensor_parallax_flag(m) != other.cdi_intra_sensor_parallax_flag(m) ||
          cdi_light_source_count(m) != other.cdi_light_source_count(m) ||
          cdi_infrared_image_present_flag(m) != other.cdi_infrared_image_present_flag(m) ||
          cdi_depth_confidence_present_flag(m) != other.cdi_depth_confidence_present_flag(m)) {
        return false;
      }
      if (cdi_depth_confidence_present_flag(m) &&
          (m_cdi_depth_confidence_flag[m] != other.m_cdi_depth_confidence_flag[m])) {
        return false;
      }
    }
  }
  return true;
}

auto CaptureDeviceInformation::operator!=(const CaptureDeviceInformation &other) const -> bool {
  return !operator==(other);
}

auto CaptureDeviceInformation::decodeFrom(Common::InputBitstream &bitstream)
    -> CaptureDeviceInformation {
  auto x = CaptureDeviceInformation{};

  x.cdi_device_model_count_minus1(bitstream.getUExpGolomb<uint16_t>());
  for (uint16_t m = 0; m <= x.cdi_device_model_count_minus1(); m++) {
    x.cdi_device_model_id(m, bitstream.readBits<uint8_t>(6));
    x.cdi_device_class_id(m, bitstream.getUExpGolomb<DeviceClassId>());

    if (x.cdi_device_class_id(m) != DeviceClassId::unknown_or_processed) {
      x.cdi_sensor_count_minus1(m, bitstream.getUExpGolomb<uint16_t>());

      for (uint16_t s = 0; s <= x.cdi_sensor_count_minus1(m); s++) {
        x.cdi_sensor_component_id(m, s, bitstream.readBits<uint8_t>(5));
      }

      x.cdi_intra_sensor_parallax_flag(m, bitstream.getFlag());
      x.cdi_light_source_count(m, bitstream.getUExpGolomb<uint16_t>());
      x.cdi_infrared_image_present_flag(m, bitstream.getFlag());
      x.cdi_depth_confidence_present_flag(m, bitstream.getFlag());

      if (x.cdi_depth_confidence_present_flag(m)) {
        for (uint16_t s = 0; s <= x.cdi_sensor_count_minus1(m); s++) {
          if (x.cdi_sensor_component_id(m, s) == 0) {
            x.cdi_depth_confidence_flag(m, s, bitstream.getFlag());
          }
        }
      }
    }
  }

  VERIFY_MIVBITSTREAM(x.checkBitstreamConstraints());
  return x;
}

void CaptureDeviceInformation::encodeTo(Common::OutputBitstream &bitstream) const {
  VERIFY_MIVBITSTREAM(checkBitstreamConstraints());
  bitstream.putUExpGolomb(cdi_device_model_count_minus1());
  for (uint16_t m = 0; m <= cdi_device_model_count_minus1(); m++) {
    bitstream.writeBits(cdi_device_model_id(m), 6);
    bitstream.putUExpGolomb(cdi_device_class_id(m));

    if (cdi_device_class_id(m) != DeviceClassId::unknown_or_processed) {
      bitstream.putUExpGolomb(cdi_sensor_count_minus1(m));
      for (uint16_t s = 0; s <= cdi_sensor_count_minus1(m); s++) {
        bitstream.writeBits(cdi_sensor_component_id(m, s), 5);
      }

      bitstream.putFlag(cdi_intra_sensor_parallax_flag(m));
      bitstream.putUExpGolomb(cdi_light_source_count(m));
      bitstream.putFlag(cdi_infrared_image_present_flag(m));
      bitstream.putFlag(cdi_depth_confidence_present_flag(m));

      if (cdi_depth_confidence_present_flag(m)) {
        for (uint16_t s = 0; s <= cdi_sensor_count_minus1(m); s++) {
          if (cdi_sensor_component_id(m, s) == 0) {
            bitstream.putFlag(cdi_depth_confidence_flag(m, s));
          }
        }
      }
    }
  }
}

auto CaptureDeviceInformation::checkBitstreamConstraints() const noexcept -> bool {
  // The value of cdi_device_model_id[ m ] shall be in the range 0 to 63, inclusive.
  for (uint8_t id : m_cdi_device_model_id) {
    if (id > 63) {
      return false;
    }
  }

  // cdi_device_model_id[ m ] shall not be equal to cdi_device_model_id[ n ] for all m != n.
  std::set<uint16_t> unique_values(m_cdi_device_model_id.begin(), m_cdi_device_model_id.end());
  return (unique_values.size() == m_cdi_device_model_id.size());
}

CaptureDeviceInformation::CaptureDeviceInformation(const Common::Json &node) {
  const auto &deviceModels = node.as<Common::Json::Array>();
  VERIFY(!deviceModels.empty());
  cdi_device_model_count_minus1(Common::downCast<uint16_t>(deviceModels.size() - 1));

  for (uint16_t m = 0; m <= cdi_device_model_count_minus1(); ++m) {
    const auto &deviceModel = deviceModels[m];

    cdi_device_model_id(m, deviceModel.require("DeviceModelId").as<uint8_t>());
    cdi_device_class_id(
        m, Common::queryEnum(deviceModel, "DeviceClassId", "Device class ID", knownDeviceClassIds));

    if (cdi_device_class_id(m) != DeviceClassId::unknown_or_processed) {
      const auto sensorCount = deviceModel.require("SensorCount").as<uint16_t>();
      cdi_sensor_count_minus1(m, Common::downCast<uint16_t>(sensorCount - 1));

      const auto sensorComponentIds = deviceModel.require("SensorComponentIds").asVector<uint8_t>();
      VERIFY(sensorComponentIds.size() == sensorCount);

      for (uint16_t s = 0; s < sensorCount; ++s) {
        cdi_sensor_component_id(m, s, sensorComponentIds[s]);
      }

      cdi_intra_sensor_parallax_flag(m, deviceModel.require("IntraSensorParallaxFlag").as<bool>());
      cdi_light_source_count(m, deviceModel.require("LightSourceCount").as<uint16_t>());
      cdi_infrared_image_present_flag(m, deviceModel.require("IrImagePresentFlag").as<bool>());

      if (const auto depthConfidence = deviceModel.optional("DepthConfidence")) {
        cdi_depth_confidence_present_flag(m, true);

        m_cdi_depth_confidence_flag[m] = depthConfidence.asVector<bool>();
        VERIFY(m_cdi_depth_confidence_flag[m].size() == sensorCount);
      }
    }
  }
}
} // namespace TMIV::MivBitstream
