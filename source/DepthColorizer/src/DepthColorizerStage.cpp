#include <TMIV/DepthColorizer/DepthColorizerStage.h>

#include <TMIV/Common/LoggingStrategy.h>

namespace TMIV::DepthColorizer {
namespace {
using MivBitstream::EncoderAtlasParams;

auto colorizedGeometryFrame(const std::vector<EncoderAtlasParams> &atlas,
                            Common::DeepFrame &frame) -> Common::DeepFrame {
  auto outframe = Common::DeepFrame{};
  outframe = frame;
  const auto width = frame.geometry.getWidth();
  const auto height = frame.geometry.getHeight();
  const auto bits =
      atlas[0].asps.asps_miv_2_extension().asme_colorized_geometry_bit_depth_minus1() + 1;

  outframe.geometry = Common::Frame<>::yuv420({width, height}, frame.geometry.getBitDepth() - bits);
  outframe.geometry.fillNeutral();
  const auto medvalue = Common::medLevel<uint16_t>(frame.geometry.getBitDepth() - bits);

  for (int64_t i = 0; i < static_cast<int64_t>(width) * static_cast<int64_t>(height); ++i) {
    const auto row = i / width;
    const auto col = i % width;
    const auto px = 0 + col;
    const auto py = 0 + row;
    outframe.geometry.getPlane(0)(py, px) = frame.geometry.getPlane(0)(py, px) >> bits;
    if (col % 2 == 0) {
      outframe.geometry.getPlane(1 + row % 2)(py / 2, px / 2) =
          Common::downCast<uint16_t>(
              ((frame.geometry.getPlane(0)(py, px) -
                ((frame.geometry.getPlane(0)(py, px) >> bits) << bits)) +
               (frame.geometry.getPlane(0)(py, px + 1) -
                ((frame.geometry.getPlane(0)(py, px + 1) >> bits) << bits))) /
              2) +
          medvalue;
    }
  }
  return outframe;
}
} // namespace

DepthColorizerStage::DepthColorizerStage(const Common::Json &componentNode)
    : m_colorizeGeometryEnabledFlag{
          componentNode.require("haveGeometryVideo").as<bool>() &&
          componentNode.require("colorizedGeometryEnabledFlag").as<bool>() &&
          !componentNode.require("framePacking").as<bool>()} {}

void DepthColorizerStage::encode(CodableUnit unit) {
  Common::logDebug("DepthColorizer stage");

  if (m_colorizeGeometryEnabledFlag) {
    for (auto &deepFrame : unit.deepFrameList) {
      deepFrame = colorizedGeometryFrame(unit.encoderParams.atlas, deepFrame);
    }
  }
  source.encode(unit);
}
} // namespace TMIV::DepthColorizer