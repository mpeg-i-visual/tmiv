/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019-2025, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <TMIV/VideoDecoder/Partition.h>

#include <TMIV/Common/Common.h>
#include <TMIV/Common/LoggingStrategy.h>
#include <TMIV/Common/verify.h>

#include <TLibDecoder/NALread.h>

#include <cstring>
#include <sstream>

using namespace std::string_literals;

namespace TMIV::VideoDecoder {
namespace {
auto nalUnitType(const std::string &unit) -> NalUnitType {
  InputNALUnit nalu;
  nalu.getBitstream().getFifo().resize(unit.size());
  memcpy(nalu.getBitstream().getFifo().data(), unit.data(), unit.size());
  read(nalu);
  return nalu.m_nalUnitType;
}

auto isIrapStart(const std::string &unit) -> bool {
  const auto nut = nalUnitType(unit);

  return (NAL_UNIT_CODED_SLICE_BLA_W_LP <= nut && nut <= NAL_UNIT_RESERVED_IRAP_VCL23) ||
         nut == NAL_UNIT_VPS || nut == NAL_UNIT_SPS;
}

auto isIrapVcf(const std::string &unit) -> bool {
  const auto nut = nalUnitType(unit);

  return NAL_UNIT_CODED_SLICE_BLA_W_LP <= nut && nut <= NAL_UNIT_RESERVED_IRAP_VCL23;
}

auto nalUnitTypeText(NalUnitType nut) -> std::string {
  switch (nut) {
#define CASE_OF(x)                                                                                 \
  case x:                                                                                          \
    return #x
    CASE_OF(NAL_UNIT_CODED_SLICE_TRAIL_N);
    CASE_OF(NAL_UNIT_CODED_SLICE_TRAIL_R);
    CASE_OF(NAL_UNIT_CODED_SLICE_TSA_N);
    CASE_OF(NAL_UNIT_CODED_SLICE_TSA_R);
    CASE_OF(NAL_UNIT_CODED_SLICE_STSA_N);
    CASE_OF(NAL_UNIT_CODED_SLICE_STSA_R);
    CASE_OF(NAL_UNIT_CODED_SLICE_RADL_N);
    CASE_OF(NAL_UNIT_CODED_SLICE_RADL_R);
    CASE_OF(NAL_UNIT_CODED_SLICE_RASL_N);
    CASE_OF(NAL_UNIT_CODED_SLICE_RASL_R);
    CASE_OF(NAL_UNIT_RESERVED_VCL_N10);
    CASE_OF(NAL_UNIT_RESERVED_VCL_R11);
    CASE_OF(NAL_UNIT_RESERVED_VCL_N12);
    CASE_OF(NAL_UNIT_RESERVED_VCL_R13);
    CASE_OF(NAL_UNIT_RESERVED_VCL_N14);
    CASE_OF(NAL_UNIT_RESERVED_VCL_R15);
    CASE_OF(NAL_UNIT_CODED_SLICE_BLA_W_LP);
    CASE_OF(NAL_UNIT_CODED_SLICE_BLA_W_RADL);
    CASE_OF(NAL_UNIT_CODED_SLICE_BLA_N_LP);
    CASE_OF(NAL_UNIT_CODED_SLICE_IDR_W_RADL);
    CASE_OF(NAL_UNIT_CODED_SLICE_IDR_N_LP);
    CASE_OF(NAL_UNIT_CODED_SLICE_CRA);
    CASE_OF(NAL_UNIT_RESERVED_IRAP_VCL22);
    CASE_OF(NAL_UNIT_RESERVED_IRAP_VCL23);
    CASE_OF(NAL_UNIT_RESERVED_VCL24);
    CASE_OF(NAL_UNIT_RESERVED_VCL25);
    CASE_OF(NAL_UNIT_RESERVED_VCL26);
    CASE_OF(NAL_UNIT_RESERVED_VCL27);
    CASE_OF(NAL_UNIT_RESERVED_VCL28);
    CASE_OF(NAL_UNIT_RESERVED_VCL29);
    CASE_OF(NAL_UNIT_RESERVED_VCL30);
    CASE_OF(NAL_UNIT_RESERVED_VCL31);
    CASE_OF(NAL_UNIT_VPS);
    CASE_OF(NAL_UNIT_SPS);
    CASE_OF(NAL_UNIT_PPS);
    CASE_OF(NAL_UNIT_ACCESS_UNIT_DELIMITER);
    CASE_OF(NAL_UNIT_EOS);
    CASE_OF(NAL_UNIT_EOB);
    CASE_OF(NAL_UNIT_FILLER_DATA);
    CASE_OF(NAL_UNIT_PREFIX_SEI);
    CASE_OF(NAL_UNIT_SUFFIX_SEI);
    CASE_OF(NAL_UNIT_RESERVED_NVCL41);
    CASE_OF(NAL_UNIT_RESERVED_NVCL42);
    CASE_OF(NAL_UNIT_RESERVED_NVCL43);
    CASE_OF(NAL_UNIT_RESERVED_NVCL44);
    CASE_OF(NAL_UNIT_RESERVED_NVCL45);
    CASE_OF(NAL_UNIT_RESERVED_NVCL46);
    CASE_OF(NAL_UNIT_RESERVED_NVCL47);
    CASE_OF(NAL_UNIT_UNSPECIFIED_48);
    CASE_OF(NAL_UNIT_UNSPECIFIED_49);
    CASE_OF(NAL_UNIT_UNSPECIFIED_50);
    CASE_OF(NAL_UNIT_UNSPECIFIED_51);
    CASE_OF(NAL_UNIT_UNSPECIFIED_52);
    CASE_OF(NAL_UNIT_UNSPECIFIED_53);
    CASE_OF(NAL_UNIT_UNSPECIFIED_54);
    CASE_OF(NAL_UNIT_UNSPECIFIED_55);
    CASE_OF(NAL_UNIT_UNSPECIFIED_56);
    CASE_OF(NAL_UNIT_UNSPECIFIED_57);
    CASE_OF(NAL_UNIT_UNSPECIFIED_58);
    CASE_OF(NAL_UNIT_UNSPECIFIED_59);
    CASE_OF(NAL_UNIT_UNSPECIFIED_60);
    CASE_OF(NAL_UNIT_UNSPECIFIED_61);
    CASE_OF(NAL_UNIT_UNSPECIFIED_62);
    CASE_OF(NAL_UNIT_UNSPECIFIED_63);
    CASE_OF(NAL_UNIT_INVALID);
#undef CASE_OF
  };

  std::ostringstream buffer;
  buffer << "nal_unit_type=" << nut;
  return buffer.str();
}

void logUnit(const std::optional<std::string> &unit) {
  if (unit) {
    std::ostringstream buffer;
    buffer << "HEVC: " << nalUnitTypeText(nalUnitType(*unit)) << " (" << unit->size() << " bytes)";
    Common::logInfo(buffer.str());
  }
}
} // namespace

auto partitionHevcMain10(NalUnitSource source) -> CodedVideoSequenceSource {
  return [source = std::make_shared<Common::ReadAhead<std::optional<std::string>>>(
              std::move(source))]() -> std::optional<std::vector<std::string>> {
    if (!source->head()) {
      return std::nullopt;
    }
    VERIFY_BITSTREAM(isIrapStart(*source->head()));

    auto sequence = std::vector<std::string>{};

    do {
      auto unit = (*source)();
      logUnit(unit);
      VERIFY_BITSTREAM(unit);
      sequence.push_back(*std::move(unit));
    } while (!isIrapVcf(sequence.back()));

    while (source->head() && !isIrapStart(*source->head())) {
      auto unit = (*source)();
      logUnit(unit);
      sequence.push_back(*std::move(unit));
    }
    return sequence;
  };
}

auto partitionHevc444(NalUnitSource source) -> CodedVideoSequenceSource {
  return partitionHevcMain10(std::move(source));
}
} // namespace TMIV::VideoDecoder
